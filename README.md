## Desafio de herois

---

**Clone o repository**
> git clone https://lucasbaldin@bitbucket.org/lucasbaldin/herois.git

**Install composer**
> composer install

**Migrate**
> php artisan migrate:refresh --seed

