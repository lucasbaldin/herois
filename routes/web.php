<?php

// Home
Route::get('/', 'HomeController@Index');

// Especialidades
Route::get('especialidades', 'EspecialidadesController@index');
Route::get('especialidades/create', 'EspecialidadesController@create');
Route::get('especialidades/{id}', 'EspecialidadesController@edit');
Route::post('especialidades', 'EspecialidadesController@save');
Route::post('especialidades/{id}', 'EspecialidadesController@update');
Route::post('especialidades/{id}/delete', 'EspecialidadesController@delete');

// Herois
Route::get('herois', 'HeroisController@index');
Route::get('herois/create', 'HeroisController@create');
Route::get('herois/{id}', 'HeroisController@edit');
Route::post('herois', 'HeroisController@save');
Route::post('herois/{id}', 'HeroisController@update');
Route::post('herois/{id}/delete', 'HeroisController@delete');

// Tipos
Route::get('tipos', 'TiposController@index');
Route::get('tipos/create', 'TiposController@create');
Route::get('tipos/{id}', 'TiposController@edit');
Route::post('tipos', 'TiposController@save');
Route::post('tipos/{id}', 'TiposController@update');
Route::post('tipos/{id}/delete', 'TiposController@delete');

Route::get('fotos/{type}/{file}', function($type, $file)
{
    $path = storage_path('app'). '\\' . $type . '\\' . $file;
    if (file_exists($path)) { 
        return Response::download($path);
    }
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
