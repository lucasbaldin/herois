<?php

Route::middleware('auth.basic')->group(function () {
    // Route::get('heroi', 'Api\HeroisController@index');
    Route::resource('tipo', 'Api\TiposController', [
        'except' => ['edit', 'create']
    ]);
    Route::resource('especialidade', 'Api\EspecialidadesController', [
        'except' => ['edit', 'create']
    ]);
    Route::resource('heroi', 'Api\HeroisController', [
        'except' => ['edit', 'create']
    ]);
});
