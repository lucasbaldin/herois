@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1>Herois</h1>
				<hr/>

				@if (Session::has('message'))
					<div class="alert alert-success">{{ Session::get('message') }}</div>
				@endif

				<div class="col-xs-6">
					<a href="{{ url('/') }}"><< Voltar</a>
				</div>
				<div class="col-xs-6 text-right">
					<a href="{{ url('/herois/create') }}" class="btn btn-primary" role="button">
						<span class="glyphicon glyphicon-plus"></span>
						<span>Cadastrar um heroi</span>
					</a>
				</div>
				<br/>

				@if (count($herois) > 0)
					<div class="row">
						<form method="GET" name="searchForm" action="{{ url('/herois') }}" accept-charset="UTF-8">
							<div class="form-group">
								<label class="control-label" for="search">Busca por nome e especialidade</label>
								<input class="form-control" name="search" type="text" value="{{ Request::get('search') }}" />
							</div>
							<div class="form-group text-right">
								<button type="submit" class="btn btn-primary">Buscar</button>
							</div>
						</form>
						<hr/>
						<div class="table-responsive">
							<table class="table table-hover">
								<thead>
									<tr>
										<th>Avatar</th>
										<th>Nome</th>
										<th>Especialidades</th>
										<th width="60">Editar</th>
										<th width="60">Excluir</th>
									</tr>
								</thead>
								<tbody>
									@foreach ($herois as $heroi)
									<tr>
										<td><img src="{{ url('/fotos') .'/'. $heroi->fotos->first()->endereco }}" class="img-responsive" /></td>
										<td>{{ $heroi->nome }}</td>
										<td>{{ $heroi->especialidades->implode('nome', ',') }}</td>
										<td width="60">
											<a href="{{ url('/herois', $heroi->id) }}" class="btn btn-primary" role="button">
												<span class="glyphicon glyphicon-pencil"></span>
											</a>
										</td>
										<td width="60">
											<form action="{{ url('/herois', $heroi->id) }}/delete" method="POST" onsubmit="return confirm('Deseja realmente excluir esse heroi?');" accept-charset="UTF-8">
												{{ csrf_field() }}
												<button type="submit" class="btn btn-primary" role="button">
													<span class="glyphicon glyphicon-trash"></span>
												</button>
											</form>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				@else
					<div class="row">
						<div class="col-xs-12">
							<h4>Nenhum heroi encontrado</h4>
						</div>
					</div>
				@endif
			</div>
		</div>
	</div>
@stop