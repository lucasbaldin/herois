@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1>Cadastro de herói</h1>
				<hr/>

				<div class="col-xs-12">
					<a href="{{ url('/herois') }}"><< Voltar</a>
				</div>
				<br/><br/>

				<div class="container-fluid">
					<div class="row">
						<div class="col-xs-12">
							<form method="POST" name="contentForm" action="{{ url('/herois', $heroi->id) }}" accept-charset="UTF-8" enctype="multipart/form-data">
								{{ csrf_field() }}
								<div class="form-group">
									<label class="control-label" for="nome">Nome</label>
									<input class="form-control" name="nome" type="text" value="{{ $heroi->nome }}" required />
								</div>
								<div class="form-group">
									<label class="control-label" for="description">Tipos</label>
									<select name="tipo" class="form-control" required>
										<option value="">Selecionar</option>
										@foreach ($tipos as $tipo)
											<option value="{{ $tipo->id }}" {{ ($heroi->tipo && $heroi->tipo->id == $tipo->id) ? 'selected="selected"' : '' }}>{{ $tipo->nome }}</option>
										@endforeach
									</select>
								</div>
								<div class="form-group">
									<label class="control-label" for="description">Especialidades</label>
									<select name="especialidades[]" multiple class="form-control" required>
										@foreach ($especialidades as $especialidade)
											<option value="{{ $especialidade->id }}" {{ (count($heroi->especialidades->where('id', $especialidade->id)) > 0) ? 'selected="selected"' : '' }}>{{ $especialidade->nome }}</option>
										@endforeach
									</select>
								</div>
								<div class="form-group">
									<label class="control-label" for="vida">Vida</label>
									<input class="form-control" name="vida" type="number" value="{{ $heroi->vida }}" required />
								</div>
								<div class="form-group">
									<label class="control-label" for="defesa">Defesa</label>
									<input class="form-control" name="defesa" type="number" value="{{ $heroi->defesa }}" required />
								</div>
								<div class="form-group">
									<label class="control-label" for="dano">Dano</label>
									<input class="form-control" name="dano" type="number" value="{{ $heroi->dano }}" required />
								</div>
								<div class="form-group">
									<label class="control-label" for="velocidade_ataque">Velocidade de ataque</label>
									<input class="form-control" name="velocidade_ataque" type="number" step="0.1" min="0" max="99" value="{{ $heroi->velocidade_ataque }}" required />
								</div>
								<div class="form-group">
									<label class="control-label" for="velocidade_movimento">Velocidade de movimento</label>
									<input class="form-control" name="velocidade_movimento" type="number" value="{{ $heroi->velocidade_movimento }}" required />
								</div>
								<div class="form-group">
									<label class="control-label" for="fotos">Fotos</label>
									<input type="file" name="fotos[]" accept="image/*" multiple {{ (!is_null($heroi->id) && count($heroi->fotos)) ? '' : 'required' }} />
								</div>
								@if (!is_null($heroi->id) && count($heroi->fotos) > 0)
									@foreach ($heroi->fotos as $foto)
										<div class="form-group col-sm-2 col-xs-6">
											<img src="{{ url('/fotos') .'/'. $foto->endereco }}" class="img-responsive" />
										</div>
									@endforeach
								@endif
								<div class="form-group">
									<div>
										<button class="btn btn-primary form-control" name="submit" type="submit">Salvar</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop
