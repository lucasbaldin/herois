@extends('layouts.app')

{{-- 
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection --}}

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<h1>Menu</h1>

				<div class="row">
					<div class="list-group col-xs-12">
						<a href="{{ url('/especialidades') }}" class="list-group-item">
							<h4 class="list-group-item-heading">Especialidades</h4>
						</a>
						<a href="{{ url('/herois') }}" class="list-group-item">
							<h4 class="list-group-item-heading">Herois</h4>
						</a>
						<a href="{{ url('/tipos') }}" class="list-group-item">
							<h4 class="list-group-item-heading">Tipos</h4>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop