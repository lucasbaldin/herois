@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1>Especialidades</h1>
				<hr/>

				@if (Session::has('message'))
					<div class="alert alert-success">{{ Session::get('message') }}</div>
				@endif

				<div class="col-xs-6">
					<a href="{{ url('/') }}"><< Voltar</a>
				</div>
				<div class="col-xs-6 text-right">
					<a href="{{ url('/especialidades/create') }}" class="btn btn-primary" role="button">
						<span class="glyphicon glyphicon-plus"></span>
						<span>Cadastrar uma especialidade</span>
					</a>
				</div>
				<br/>

				@if (count($especialidades) > 0)
					<div class="row">
						<form method="GET" name="searchForm" action="{{ url('/especialidades') }}" accept-charset="UTF-8">
							<div class="form-group">
								<label class="control-label" for="search">Busca por nome</label>
								<input class="form-control" name="search" type="text" value="{{ Request::get('search') }}" />
							</div>
							<div class="form-group text-right">
								<button type="submit" class="btn btn-primary">Buscar</button>
							</div>
						</form>
						<hr/>
						<div class="table-responsive">
							<table class="table table-hover">
								<thead>
									<tr>
										<th>Nome</th>
										<th width="60">Editar</th>
										<th width="60">Excluir</th>
									</tr>
								</thead>
								<tbody>
									@foreach ($especialidades as $especialidade)
									<tr>
										<td>{{ $especialidade->nome }}</td>
										<td width="60">
											<a href="{{ url('/especialidades', $especialidade->id) }}" class="btn btn-primary" role="button">
												<span class="glyphicon glyphicon-pencil"></span>
											</a>
										</td>
										<td width="60">
											<form action="{{ url('/especialidades', $especialidade->id) }}/delete" method="POST" onsubmit="return confirm('Deseja realmente excluir essa especialidade?');" accept-charset="UTF-8">
												{{ csrf_field() }}
												<button type="submit" class="btn btn-primary" role="button">
													<span class="glyphicon glyphicon-trash"></span>
												</button>
											</form>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				@else
					<div class="row">
						<div class="col-xs-12">
							<h4>Nenhuma especialidade encontrada</h4>
						</div>
					</div>
				@endif
			</div>
		</div>
	</div>
@stop