@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1>Cadastro de especialidade</h1>
				<hr/>

				<div class="col-xs-12">
					<a href="{{ url('/especialidades') }}"><< Voltar</a>
				</div>
				<br/><br/>

				<div class="container-fluid">
					<div class="row">
						<div class="col-xs-12">
							<form method="POST" name="contentForm" action="{{ url('/especialidades', $especialidade->id) }}" accept-charset="UTF-8" enctype="multipart/form-data">
								{{ csrf_field() }}
								<div class="form-group">
									<label class="control-label" for="name">Nome</label>
									<input class="form-control" id="nome" name="nome" type="text" value="{{ $especialidade->nome }}" required />
								</div>
								<div class="form-group">
									<div>
										<button class="btn btn-primary form-control" name="submit" type="submit">Salvar</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
 @stop
