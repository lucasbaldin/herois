<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Especialidade extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nome'
    ];
    
    protected $dates = ['deleted_at'];

	public function herois()
	{
		return $this->belongsToMany('App\Heroi')
			->withTimestamps();
	}
}
