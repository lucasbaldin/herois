<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clan extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nome'
    ];
    
    protected $dates = ['deleted_at'];
}
