<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HeroiFoto extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $fillable = [
		'endereco',
		'heroi_id'
	];
 
    protected $dates = ['deleted_at'];
    
    public function heroi()
    {
        return $this->belongsTo('App\Heroi');
    }
}
