<?php

namespace App\Http\Controllers\Api;

use App\Tipo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TiposController extends Controller
{
    public function index()
	{
        $tipos = Tipo::all();
        return response()->json($tipos);
	}
	
    public function show($id)
    {
        try {
			$tipo = Tipo::findOrFail($id);
			return response()->json($tipo);
		} catch (Illuminate\Database\Eloquent\ModelNotFoundException $e) {
			response()->json($e);
		}
	}
	
    public function destroy($id)
    {
        try {
			$tipo = Tipo::findOrFail($id);
			$tipo->delete();
		} catch (Illuminate\Database\Eloquent\ModelNotFoundException $e) {
			response()->json($e);
		}
	}
	
    public function store(Request $request)
    {
        try {
			$tipo = Tipo::create($request->all());

			return response()->json($tipo, 201);
		} catch (Illuminate\Database\Eloquent\ModelNotFoundException $e) {
			response()->json($e);
		}
    }
	
    public function update(Request $request, $id)
    {
        try {
			$tipo = Tipo::findOrFail($id);

			$tipo->update($request->all());

            return response()->json($tipo);
        } catch (Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            response()->json($e);
        }
    }
}
