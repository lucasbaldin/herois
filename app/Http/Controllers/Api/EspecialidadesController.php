<?php

namespace App\Http\Controllers\Api;

use App\Especialidade;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EspecialidadesController extends Controller
{
    public function index()
	{
        $especialidades = Especialidade::all();
        return response()->json($especialidades);
	}
	
    public function show($id)
    {
        try {
			$especialidade = Especialidade::findOrFail($id);
			return response()->json($especialidade);
		} catch (Illuminate\Database\Eloquent\ModelNotFoundException $e) {
			response()->json($e);
		}
	}
	
    public function destroy($id)
    {
        try {
			$especialidade = Especialidade::findOrFail($id);
			$especialidade->delete();
		} catch (Illuminate\Database\Eloquent\ModelNotFoundException $e) {
			response()->json($e);
		}
	}
	
    public function store(Request $request)
    {
        try {
			$especialidade = Especialidade::create($request->all());

			return response()->json($especialidade, 201);
		} catch (Illuminate\Database\Eloquent\ModelNotFoundException $e) {
			response()->json($e);
		}
    }
	
    public function update(Request $request, $id)
    {
        try {
			$especialidade = Especialidade::findOrFail($id);

			$especialidade->update($request->all());

            return response()->json($especialidade);
        } catch (Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            response()->json($e);
        }
    }
}
