<?php

namespace App\Http\Controllers\Api;

use App\Heroi;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HeroisController extends Controller
{
    public function index()
	{
        $herois = Heroi::with(['fotos', 'tipo', 'especialidades'])->get();
        return response()->json($herois);
	}
	
    public function show($id)
    {
        try {
			$heroi = Heroi::with(['fotos', 'tipo', 'especialidades'])->findOrFail($id);
			return response()->json($heroi);
		} catch (Illuminate\Database\Eloquent\ModelNotFoundException $e) {
			response()->json($e);
		}
	}
	
    public function destroy($id)
    {
        try {
			$heroi = Heroi::findOrFail($id);
			$heroi->delete();
		} catch (Illuminate\Database\Eloquent\ModelNotFoundException $e) {
			response()->json($e);
		}
	}
	
    public function store(Request $request)
    {
        try {
			$heroi = Heroi::create($request->all());
	
			if (count($request->especialidades) > 0) {
				$heroi->especialidades()->attach($request->especialidades);
			}
	
			if (count($request->fotos) > 0) {
				foreach ($request->fotos as $foto) {
					$endereco = $foto->store('herois');
					HeroiFoto::create([
						'heroi_id' => $heroi->id,
						'endereco' => $endereco
					]);
				}
			}

			return response()->json($heroi, 201);
		} catch (Illuminate\Database\Eloquent\ModelNotFoundException $e) {
			response()->json($e);
		}
    }
	
    public function update(Request $request, $id)
    {
        try {
			$heroi = Heroi::findOrFail($id);
	
			$heroi->update($request->all());
	
			if (count($request->especialidades) > 0) {
				$heroi->especialidades()->sync($request->especialidades);
			}
	
			if (count($request->fotos) > 0) {
				$fotos = HeroiFoto::all()->where('heroi_id', $heroi->id);
				
				if (count($fotos) > 0) {
					foreach ($fotos as $foto) {
						if (Storage::exists($foto->endereco)) {
							Storage::Delete($foto->endereco);
						}
						$foto->delete();
					}
				}
	
				foreach ($request->fotos as $foto) {
					$endereco = $foto->store('herois');
					HeroiFoto::create([
						'heroi_id' => $heroi->id,
						'endereco' => $endereco
					]);
				}
			}

            return response()->json($heroi);
        } catch (Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            response()->json($e);
        }
    }
}
