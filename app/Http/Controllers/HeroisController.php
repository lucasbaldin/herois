<?php

namespace App\Http\Controllers;

use Request;
use App\Tipo;
use App\Heroi;
use App\Especialidade;
use App\HeroiFoto;
use App\Http\Requests\HeroiRequest;
use Illuminate\Support\Facades\Storage;

class HeroisController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
	
	public function index()
	{
		$inputs = Request::all();

		if (count($inputs) > 0) {
			$search = $inputs['search'];
			$herois = Heroi::whereHas('especialidades', function ($query) use ($search) {
					$query->where('nome', 'like', '%' . $search . '%');
				})
				->orWhere('nome', 'like', '%' . $search. '%')
				->get();
		} else {
			$herois = Heroi::all();
		}

		return View('heroi.index', compact('herois'));
	}

	public function create()
	{
		$heroi = new Heroi;

		$tipos = Tipo::all();

		$especialidades = Especialidade::all();

		return View('heroi.create', compact('heroi', 'tipos', 'especialidades'));
	}

	public function edit($id)
	{
		$heroi = Heroi::findOrFail($id);

		$tipos = Tipo::all();
		
		$especialidades = Especialidade::all();

		return View('heroi.create', compact('heroi', 'tipos', 'especialidades'));
	}

	public function save(HeroiRequest $request)
	{
		$heroi = Heroi::create($request->all());

        if (count($request->especialidades) > 0) {
			$heroi->especialidades()->attach($request->especialidades);
		}

		if (count($request->fotos) > 0) {
			foreach ($request->fotos as $foto) {
				$endereco = $foto->store('herois');
				HeroiFoto::create([
					'heroi_id' => $heroi->id,
					'endereco' => $endereco
				]);
			}
		}

		\Session::flash('message', 'Heroi cadastrado com sucesso!');

		return redirect('herois');
	}

	public function update(HeroiRequest $request, $id)
	{
		$heroi = Heroi::findOrFail($id);

		$heroi->update($request->all());

        if (count($request->especialidades) > 0) {
			$heroi->especialidades()->sync($request->especialidades);
		}

		if (count($request->fotos) > 0) {
			$fotos = HeroiFoto::all()->where('heroi_id', $heroi->id);
			
			if (count($fotos) > 0) {
				foreach ($fotos as $foto) {
					if (Storage::exists($foto->endereco)) {
						Storage::Delete($foto->endereco);
					}
					$foto->delete();
				}
			}

			foreach ($request->fotos as $foto) {
				$endereco = $foto->store('herois');
				HeroiFoto::create([
					'heroi_id' => $heroi->id,
					'endereco' => $endereco
				]);
			}
		}

		\Session::flash('message', 'Heroi atualizado com sucesso!');

		return redirect('herois');
	}

	public function delete($id)
	{
		$heroi = Heroi::findOrFail($id);

		$fotos = HeroiFoto::all()->where('heroi_id', $heroi->id);

		if (count($fotos) > 0) {
			foreach ($fotos as $foto) {
				if (Storage::exists($foto->endereco)) {
					Storage::Delete($foto->endereco);
				}
				$foto->delete();
			}
		}

		$heroi->delete();

		\Session::flash('message', 'Heroi excluído com sucesso!');

		return redirect('herois');
	}
}
