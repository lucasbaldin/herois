<?php

namespace App\Http\Controllers;

use Request;
use App\Tipo;
use App\Http\Requests\TipoRequest;
use Illuminate\Support\Facades\Storage;

class TiposController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
	
    public function index()
    {
		$inputs = Request::all();

		if (count($inputs) > 0) {
			$tipos = Tipo::where('nome', 'like', '%' . $inputs['search'] . '%')->get();
		} else {
			$tipos = Tipo::all();
		}

    	return View('tipo.index', compact('tipos'));
    }

    public function create()
    {
    	$tipo = new Tipo;

    	return View('tipo.create', compact('tipo'));
    }

    public function edit($id)
    {
    	$tipo = Tipo::findOrFail($id);

    	return View('tipo.create', compact('tipo'));
    }

    public function save(TipoRequest $request)
    {
    	Tipo::create($request->all());

		\Session::flash('message', 'Tipo cadastrado com sucesso!');

    	return redirect('tipos');
    }

  	public function update(TipoRequest $request, $id)
  	{
  		$tipo = Tipo::findOrFail($id);

  		$tipo->update($request->all());

		\Session::flash('message', 'Tipo atualizado com sucesso!');

  		return redirect('tipos');
  	}

	public function delete($id)
	{
		$tipo = Tipo::findOrFail($id);

		$tipo->delete();

		\Session::flash('message', 'Tipo excluído com sucesso!');

		return redirect('tipos');
	}
}
