<?php

namespace App\Http\Controllers;

use Request;
use App\Especialidade;
use App\Http\Requests\EspecialidadeRequest;
use Illuminate\Support\Facades\Storage;

class EspecialidadesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
	
    public function index()
    {
		$inputs = Request::all();

		if (count($inputs) > 0) {
			$especialidades = Especialidade::where('nome', 'like', '%' . $inputs['search'] . '%')->get();
		} else {
			$especialidades = Especialidade::all();
		}

    	return View('especialidade.index', compact('especialidades'));
    }

    public function create()
    {
    	$especialidade = new Especialidade;

    	return View('especialidade.create', compact('especialidade'));
    }

    public function edit($id)
    {
    	$especialidade = Especialidade::findOrFail($id);

    	return View('especialidade.create', compact('especialidade'));
    }

    public function save(EspecialidadeRequest $request)
    {
    	Especialidade::create($request->all());

		\Session::flash('message', 'Especialidade cadastrada com sucesso!');

    	return redirect('especialidades');
    }

  	public function update(EspecialidadeRequest $request, $id)
  	{
  		$especialidade = Especialidade::findOrFail($id);

  		$especialidade->update($request->all());

		\Session::flash('message', 'Especialidade atualizada com sucesso!');

  		return redirect('especialidades');
  	}

	public function delete($id)
	{
		$especialidade = Especialidade::findOrFail($id);

		$especialidade->delete();

		\Session::flash('message', 'Especialidade excluída com sucesso!');

		return redirect('especialidades');
	}
}
