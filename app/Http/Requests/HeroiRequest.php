<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class HeroiRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules =  [
            'nome' => 'required',
            'vida' => 'required',
            'defesa' => 'required',
            'dano' => 'required',
            'velocidade_ataque' => 'required',
            'velocidade_movimento' => 'required',
        ];
		
        $fotos = count($this->input('fotos'));

		for ($index = 0; $index < $fotos; $index++) {
        // foreach(range(0, $fotos) as $index) {
            $rules['fotos.' . $index] = 'image|mimes:*';
        }

        return $rules;
    }
}
