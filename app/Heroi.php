<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Heroi extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nome',
        'vida',
        'defesa',
        'dano',
        'velocidade_ataque',
        'velocidade_movimento'
    ];
    
    protected $dates = ['deleted_at'];

	public function tipo()
	{
		return $this->belongsTo('App\Tipo');
	}

	public function especialidades()
	{
		return $this->belongsToMany('App\Especialidade')
			->withTimestamps();
	}

	public function fotos()
	{
		return $this->hasMany('App\HeroiFoto');
	}
}
