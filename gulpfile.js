var elixir = require('laravel-elixir');

elixir(mix => {
    mix.copy('node_modules/bootstrap-sass/assets/fonts/bootstrap/','public/css/fonts/bootstrap');
    mix.sass('app.scss')
       .webpack('app.js');
});