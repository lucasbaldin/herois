<?php

use Illuminate\Database\Seeder;

class EspecialidadesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Especialidade::create(['nome' => 'Magia branca']);
        App\Especialidade::create(['nome' => 'Cura']);
        App\Especialidade::create(['nome' => 'Tanker']);
        App\Especialidade::create(['nome' => 'Invocação']);
        App\Especialidade::create(['nome' => 'Ataque à distância']);
        App\Especialidade::create(['nome' => 'Matador de chefes']);
        App\Especialidade::create(['nome' => 'Antitanque']);
        App\Especialidade::create(['nome' => 'Ataque em área']);
    }
}