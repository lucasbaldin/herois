<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class HeroisTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $heroi = App\Heroi::create([
            'nome' => 'Steven',
            'vida' => 2900,
            'defesa' => 200,
            'dano' => 340,
            'velocidade_ataque' => 1.3,
            'velocidade_movimento' => 320,
            'tipo_id' => 1
        ]);
        $heroi->especialidades()->attach([1]);
        $this->insertFotos($heroi);
        
        $heroi = App\Heroi::create([
            'nome' => 'Mona',
            'vida' => 3100,
            'defesa' => 200,
            'dano' => 180,
            'velocidade_ataque' => 1.3,
            'velocidade_movimento' => 330,
            'tipo_id' => 2
        ]);
        $heroi->especialidades()->attach([1,2]);
        $this->insertFotos($heroi);
        
        $heroi = App\Heroi::create([
            'nome' => 'Morgan',
            'vida' => 6000,
            'defesa' => 360,
            'dano' => 130,
            'velocidade_ataque' => 1.1,
            'velocidade_movimento' => 300,
            'tipo_id' => 3
        ]);
        $heroi->especialidades()->attach([3]);
        $this->insertFotos($heroi);
        
        $heroi = App\Heroi::create([
            'nome' => 'Rank',
            'vida' => 2500,
            'defesa' => 220,
            'dano' => 300,
            'velocidade_ataque' => 1.2,
            'velocidade_movimento' => 330,
            'tipo_id' => 1
        ]);
        $heroi->especialidades()->attach([4]);
        $this->insertFotos($heroi);
        
        $heroi = App\Heroi::create([
            'nome' => 'Braian',
            'vida' => 2400,
            'defesa' => 190,
            'dano' => 330,
            'velocidade_ataque' => 1.8,
            'velocidade_movimento' => 320,
            'tipo_id' => 4
        ]);
        $heroi->especialidades()->attach([5]);
        $this->insertFotos($heroi);
        
        $heroi = App\Heroi::create([
            'nome' => 'Lariel',
            'vida' => 3800,
            'defesa' => 250,
            'dano' => 280,
            'velocidade_ataque' => 1.5,
            'velocidade_movimento' => 365,
            'tipo_id' => 5
        ]);
        $heroi->especialidades()->attach([6,7]);
        $this->insertFotos($heroi);
        
        $heroi = App\Heroi::create([
            'nome' => 'Maycon',
            'vida' => 3400,
            'defesa' => 260,
            'dano' => 290,
            'velocidade_ataque' => 1.4,
            'velocidade_movimento' => 365,
            'tipo_id' => 1
        ]);
        $heroi->especialidades()->attach([6]);
        $this->insertFotos($heroi);
        
        $heroi = App\Heroi::create([
            'nome' => 'Rock',
            'vida' => 5600,
            'defesa' => 400,
            'dano' => 150,
            'velocidade_ataque' => 1.0,
            'velocidade_movimento' => 300,
            'tipo_id' => 3
        ]);
        $heroi->especialidades()->attach([3]);
        $this->insertFotos($heroi);
        
        $heroi = App\Heroi::create([
            'nome' => 'Rakan',
            'vida' => 3000,
            'defesa' => 250,
            'dano' => 400,
            'velocidade_ataque' => 1.5,
            'velocidade_movimento' => 325,
            'tipo_id' => 6
        ]);
        $heroi->especialidades()->attach([7]);
        $this->insertFotos($heroi);
        
        $heroi = App\Heroi::create([
            'nome' => 'Gruntar',
            'vida' => 3700,
            'defesa' => 240,
            'dano' => 190,
            'velocidade_ataque' => 1.4,
            'velocidade_movimento' => 345,
            'tipo_id' => 5
        ]);
        $heroi->especialidades()->attach([8]);
        $this->insertFotos($heroi);
    }

    public function insertFotos($heroi) {
        $endereco = 'herois/';
        if (!Storage::exists($endereco)) {
            Storage::makeDirectory($endereco);
        }

        if (Storage::exists($endereco . strtolower($heroi->nome))) {
            $fotos = Storage::files($endereco . strtolower($heroi->nome));
            if (count($fotos) > 0) {
                foreach ($fotos as $foto) {
                    $file = $endereco . md5($foto) . '.' . pathinfo($foto, PATHINFO_EXTENSION);
                    if (!Storage::exists($file)) {
                        Storage::copy($foto, $file);
                    }
                    App\HeroiFoto::create([
                        'heroi_id' => $heroi->id,
                        'endereco' => $file
                    ]);
                }
            }
        }
    }
}