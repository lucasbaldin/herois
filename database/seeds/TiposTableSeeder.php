<?php

use Illuminate\Database\Seeder;

class TiposTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Tipo::create(['nome' => 'Mago']);
        App\Tipo::create(['nome' => 'Sacerdote']);
        App\Tipo::create(['nome' => 'Lutador']);
        App\Tipo::create(['nome' => 'Arqueiro']);
        App\Tipo::create(['nome' => 'Cavaleiro']);
        App\Tipo::create(['nome' => 'Espadachim']);
    }
}