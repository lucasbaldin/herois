<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHeroisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('herois', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            
            $table->increments('id');
            $table->string('nome');
            $table->integer('vida');
            $table->integer('defesa');
            $table->integer('dano');
            $table->integer('velocidade_movimento');
            $table->float('velocidade_ataque', 10, 1);
            
			$table->integer('tipo_id')->unsigned()->nullable();
			$table->foreign('tipo_id')->references('id')->on('tipos')->onDelete('cascade');
            
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('herois', function (Blueprint $table) {
            $table->dropForeign('herois_tipo_id_foreign');
            $table->dropColumn('tipo_id');
        });

        Schema::dropIfExists('herois');
    }
}
