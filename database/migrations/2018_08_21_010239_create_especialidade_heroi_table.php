<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEspecialidadeHeroiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('especialidade_heroi', function(Blueprint $table)
		{
            $table->engine = 'InnoDB';
            
			$table->integer('especialidade_id')->unsigned()->nullable();
			$table->foreign('especialidade_id')->references('id')
					->on('especialidades')->onDelete('cascade');

			$table->integer('heroi_id')->unsigned()->nullable();
			$table->foreign('heroi_id')->references('id')
					->on('herois')->onDelete('cascade');

            $table->primary(['especialidade_id','heroi_id']);
			$table->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('especialidade_heroi', function (Blueprint $table) {
            $table->dropForeign('especialidade_heroi_especialidade_id_foreign');
            $table->dropForeign('especialidade_heroi_heroi_id_foreign');
            $table->dropColumn('especialidade_id');
            $table->dropColumn('heroi_id');
        });

        Schema::dropIfExists('especialidade_heroi');
    }
}
